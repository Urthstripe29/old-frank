SpriteKit Game
==============

This game will be a farming adventure game similar to Harvest Moon, Zelda, and Rune Factory. It is just for fun and may never see the light of day on the app store.

Updates, videos, and images of the work in progress can be seen on my facebook page. https://www.facebook.com/skymistdevelopment

To Do
-----
* Switch Inventory view from UIKit to SpriteKit
* Sketch out a starting "world" map
* Create new tools, Hoe, Watering Can, Axe...etc.


Done
----

