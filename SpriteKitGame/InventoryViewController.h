//
//  InventoryViewController.h
//  SpriteKitGame
//
//  Created by Skyler Lauren on 1/4/15.
//  Copyright (c) 2015 Sky Mist Development. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"

@interface InventoryViewController : UIViewController

@property (nonatomic, strong)Player *player;

@end
